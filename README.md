# SQLite Utility

Simple editing-tool for SQLite-Databases based on JavaFx 8 and the [sqlite-jdbc](https://github.com/xerial/sqlite-jdbc).

## Features

* Show the content of database tables
* Editing
    * Add, remove or rename tables
    * Add or remove rows/columns
    * Modify entries
* Execute custom SQL queries
* Localization (English, German)

## Build & Distribute

This project uses the [Gradle](https://gradle.org) build system.
A wrapper for it is contained in this project, but if you want to install Gradle globally see this [guide](https://gradle.org/install/).

### Build and Execute

```bash
./gradlew run # unix
gradlew run # windows
```

> If Gradle is already installed on your device, you can use `gradle run` instead.

### Native Application Package

```bash
./gradlew jfxNative # unix
gradlew jfxNative # windows
```

This creates a native application package (.exe on Windows, .app on MacOs...) which is located at `build/jfx/native`.

### Jar Package

```bash
./gradlew jarAll # unix
gradlew jarAll # windows
```

This creates a single jar, which contains all dependencies. The jar file is located at `build/libraries/sqliteutility-all.jar`.

### Libraries

* [org.xerial:sqlite-jdbc@3.20.0](https://mvnrepository.com/artifact/org.xerial/sqlite-jdbc/3.20.0)
* [org.fxmisc.richtext:richtextfx@0.7-M5](https://mvnrepository.com/artifact/org.fxmisc.richtext/richtextfx/0.7-M5)
* [junit:junit@4.12](https://mvnrepository.com/artifact/junit/junit/4.12)

## Documentation

The latest version of the Javadoc ([page](https://l4r0x.gitlab.io/sqlite-utility/)) is in the [/build/docs/javadoc](/build/docs/javadoc) directory.

### See Also

* [Java 8 API](https://docs.oracle.com/javase/8/docs/api/)
    * [JavaFX 8](https://docs.oracle.com/javase/8/javafx/api/)
* [SQLite JDBC](https://bitbucket.org/xerial/sqlite-jdbc/wiki/Home)
    * [SQLite](https://www.sqlite.org/docs.html)

## License

See [LICENSE](/LICENSE).
