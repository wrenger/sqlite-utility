/*
 * Copyright (C) 2016 Lars Wrenger
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package sqliteutility.gui;

import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.*;
import javafx.scene.control.*;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.layout.GridPane;
import javafx.stage.*;
import javafx.util.Pair;
import sqliteutility.gui.sqltable.NullStringConverter;

import java.io.*;
import java.util.Optional;

/**
 * Helper class for basic dialogs.
 *
 * @author Lars Wrenger
 */
public class AlertUtility
{

    private AlertUtility() {}

    /**
     * Shows an error dialog which displays a exception including it's stack trace.
     *
     * @param title Title for the dialog
     * @param e     Exception that should be displayed
     */
    public static void exceptionDialog(String title, Exception e)
    {
        StringWriter sw = new StringWriter();
        e.printStackTrace(new PrintWriter(sw));

        TextArea textArea = new TextArea(sw.toString());
        textArea.setEditable(false);
        textArea.setWrapText(true);

        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle(title);
        alert.setHeaderText(title);
        alert.setContentText(e.getClass().getName() + ":\n" + e.getMessage() + "\n");
        alert.getDialogPane().setExpandableContent(textArea);

        alert.showAndWait();
    }

    /**
     * Shows a information dialog.
     *
     * @param title   Title of the dialog
     * @param message Content of the dialog
     */
    public static void infoDialog(String title, String message)
    {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle(title);
        alert.setHeaderText(null);
        alert.setContentText(message);

        alert.showAndWait();
    }

    /**
     * Shows a confirmation dialog and returns if the user accepted.
     *
     * @param title   Title of the dialog
     * @param message Content of the dialog
     *
     * @return if the user accepted
     */
    public static boolean questionDialog(String title, String message)
    {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle(title);
        alert.setHeaderText(null);
        alert.setContentText(message);

        Optional<ButtonType> result = alert.showAndWait();
        return result.filter(buttonType -> buttonType == ButtonType.OK).isPresent();
    }

    /**
     * Shows a text input dialog and returns the user input.
     *
     * @param title   Title of the dialog
     * @param content Content of the dialog
     *
     * @return The user input or <code>null</code> if the user cancels
     */
    public static String inputDialog(String title, String content)
    {
        TextInputDialog tid = new TextInputDialog();
        tid.setTitle(title);
        tid.setHeaderText(null);
        tid.setContentText(content);

        Optional<String> result = tid.showAndWait();
        return result.orElse(null);
    }

    /**
     * Dialog for adding new table columns.
     *
     * @param title      Title of the dialog
     * @param namePrompt Prompt text for the name field
     * @param typePrompt Prompt text for the sql type field
     *
     * @return Pair of column name (key) and sql data type (value)
     */
    public static Pair<String, String> columnDialog(
            String title, String namePrompt, String typePrompt)
    {
        TextField name = new TextField();
        name.setPromptText(namePrompt);

        TextField type = new TextField();
        type.setPromptText(typePrompt);

        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.add(name, 0, 0);
        grid.add(type, 1, 0);

        Dialog<Pair<String, String>> dialog = new Dialog<>();
        dialog.setTitle(title);
        dialog.setHeaderText(null);
        dialog.getDialogPane().getButtonTypes().addAll(ButtonType.APPLY, ButtonType.CANCEL);
        dialog.getDialogPane().setContent(grid);

        dialog.setResultConverter(dialogButton ->
                (dialogButton == ButtonType.APPLY) ? new Pair<>(name.getText(), type.getText())
                        : null);

        Optional<Pair<String, String>> result = dialog.showAndWait();
        return result.orElse(null);
    }

    /**
     * Dialog for adding new table rows.
     *
     * @param title   title of the dialog
     * @param columns column-names
     *
     * @return values for the new table row
     */
    public static Object[] rowDialog(String title, String[] columns)
    {
        TableView<ObservableList<Object>> table = new TableView<>();
        ObservableList<Object> values = FXCollections.observableArrayList();

        for (int i = 0; i < columns.length; i++) {
            final int j = i;

            TableColumn<ObservableList<Object>, Object> tc = new TableColumn<>(columns[i]);

            tc.setCellValueFactory(
                    (param) -> new SimpleObjectProperty<>(String.valueOf(param.getValue().get(j))));
            tc.setCellFactory(TextFieldTableCell.forTableColumn(new NullStringConverter()));
            tc.setOnEditCommit((param) -> table.getItems().get(0).set(j, param.getNewValue()));

            table.getColumns().add(tc);
            values.add("");
        }
        table.getItems().add(values);
        table.setEditable(true);
        table.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        table.setPrefSize(500.0, 100.0);

        Dialog<Object[]> dialog = new Dialog<>();
        dialog.setTitle(title);
        dialog.setHeaderText(null);
        dialog.getDialogPane().getButtonTypes().addAll(ButtonType.APPLY, ButtonType.CANCEL);
        dialog.getDialogPane().setContent(table);
        dialog.setResultConverter(dialogButton -> (dialogButton == ButtonType.APPLY)
                ? values.toArray(new Object[values.size()]) : null);

        Optional<Object[]> result = dialog.showAndWait();

        return result.orElse(null);
    }

    private static FileChooser databaseChooser(String title)
    {
        FileChooser chooser = new FileChooser();
        chooser.setTitle(title);
        chooser.getExtensionFilters().add(new FileChooser.ExtensionFilter(
                "Database", "*.db"));
        return chooser;
    }

    /**
     * Shows a file chooser for database files and returns the selected file.
     *
     * @param stage Parent stage
     * @param title Title of the dialog stage
     *
     * @return the selected file or <code>null</code> if the user cancels.
     */
    public static File openDatabaseDialog(Stage stage, String title)
    {
        return databaseChooser(title).showOpenDialog(stage);
    }

    public static File saveDatabaseDialog(Stage stage, String title)
    {
        return databaseChooser(title).showSaveDialog(stage);
    }
}
