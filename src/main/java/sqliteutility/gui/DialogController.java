package sqliteutility.gui;

import javafx.stage.*;

import java.io.IOException;
import java.util.ResourceBundle;

abstract public class DialogController
        extends WindowController
{
    protected DialogController(Stage stage, ResourceBundle resources, Stage owner)
            throws IOException
    {
        super(stage, resources);
        setupDialog(owner);
    }

    private void setupDialog(Stage owner)
            throws IOException
    {
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.initOwner(owner);
    }

    /**
     * Shows this window and waits for is to be hidden before returning to the caller.
     */
    public void showAndWait()
    {
        stage.showAndWait();
    }
}
