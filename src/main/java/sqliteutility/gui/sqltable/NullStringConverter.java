/*
 * Copyright (C) 2016 Lars Wrenger
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package sqliteutility.gui.sqltable;

import javafx.util.StringConverter;

/**
 * {@link StringConverter} implementation for {@link String} values.
 *
 * @author Lars Wrenger
 */
public class NullStringConverter
        extends StringConverter<Object>
{
    @Override
    public String toString(Object value)
    {
        return String.valueOf(value);
    }

    @Override
    public Object fromString(String value)
    {
        return value != null && !value.equals("null") ? value : null;
    }
}