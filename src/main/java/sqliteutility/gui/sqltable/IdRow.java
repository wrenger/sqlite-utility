package sqliteutility.gui.sqltable;

import javafx.beans.property.*;

import java.util.AbstractList;
import java.util.stream.Stream;

public class IdRow
        extends AbstractList<Object>
{
    private final ObjectProperty<Object>[] attributes;
    private final IntegerProperty id;

    public IdRow(boolean withId, Object... attributes)
    {
        if (withId) {
            Object[] values = new Object[attributes.length - 1];
            System.arraycopy(attributes, 1, values, 0, attributes.length - 1);
            this.attributes = properties(values);
            this.id = new SimpleIntegerProperty((Integer) attributes[0]);
        }
        else {
            this.attributes = properties(attributes);
            this.id = null;
        }
    }

    @SuppressWarnings("unchecked")
    private static ObjectProperty<Object>[] properties(Object... values) {
        return Stream.of(values).map(SimpleObjectProperty::new).toArray(SimpleObjectProperty[]::new);
    }

    public boolean hasId() {
        return id != null;
    }

    public int getId() {
        if (!hasId()) return 0;
        return id.get();
    }

    public IntegerProperty idProperty() {
        return id;
    }

    public ObjectProperty<Object> property(int index) {
        return attributes[index];
    }

    @Override
    public Object[] toArray()
    {
        Object[] out = new Object[size()];
        for(int i = 0; i < out.length; i++) {
            out[i] = get(i);
        }
        return out;
    }

    @Override
    public Object get(int index)
    {
        return attributes[index].get();
    }

    @Override
    public Object set(int index, Object element)
    {
        Object recent = get(index);
        attributes[index].set(element);
        return recent;
    }

    @Override
    public int size()
    {
        return attributes.length;
    }

    @Override
    public String toString()
    {
        return "id=" + getId() + ", " + super.toString();
    }
}
