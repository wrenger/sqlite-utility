/*
 * Copyright (C) 2016 Lars Wrenger
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package sqliteutility.gui;

import javafx.collections.*;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.input.*;
import javafx.stage.Stage;
import javafx.util.Pair;
import org.fxmisc.richtext.CodeArea;
import sqliteutility.PersistentData;
import sqliteutility.gui.sqltable.*;
import sqliteutility.sql.*;

import java.io.*;
import java.sql.SQLException;
import java.util.*;
import java.util.stream.*;

/**
 * FXML Controller class
 *
 * @author Lars Wrenger
 */
public class MainController
        extends WindowController
{
    private static final int MAX_ROW_COUNT = 5000;

    @FXML private ComboBox<String> tableSelector;
    @FXML private CodeArea sqlField;
    @FXML private TableView<IdRow> resultTable;
    @FXML private Label statusLabel;
    @FXML private Menu recentFilesMenu;
    @FXML private MenuBar menuBar;
    @FXML private ContextMenu resultMenu;
    @FXML private MenuItem deleteRowMenu;
    @FXML private MenuItem copySelectionMenu;

    private RecentFilesMenu recentFiles;

    public MainController(Stage stage, ResourceBundle resources)
            throws IOException
    {
        super(stage, resources);

        loadRecentFiles();
        initialize();
        File current = recentFiles.current();
        if (current != null)
            openDatabase(current);
    }

    public MainController(Stage stage, ResourceBundle resources, File database)
            throws IOException
    {
        super(stage, resources);

        loadRecentFiles();
        initialize();
        openDatabase(database);
    }

    private void loadRecentFiles()
    {
        recentFiles = new RecentFilesMenu(recentFilesMenu, this::openDatabase);
        recentFiles.setAll(PersistentData.loadLastFiles());
    }

    private void initialize()
    {
        resultTable.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        resultTable.getSelectionModel().setCellSelectionEnabled(true);

        menuBar.setUseSystemMenuBar(true);

        sqlField.richChanges().filter(ch -> !ch.getInserted().equals(ch.getRemoved()))
                .subscribe(change -> sqlField.setStyleSpans(0,
                        SQLHighlighter.computeHighlighting(sqlField.getText())));
    }

    @Override
    protected String getUrl()
    {
        return "MainWindow.fxml";
    }


    /**
     * Returns the most recently opened files (including the currently openDatabase file).
     *
     * @return list of most recently opened files
     */
    public Collection<File> getRecentFiles()
    {
        return recentFiles.toList();
    }


    // <editor-fold defaultstate="collapsed" desc="EventHandler">
    @FXML
    private void schemaAction()
    {
        execute(SQLUtility.sqliteSchema(), null);
    }

    @FXML
    public void onSelectTable()
    {
        if (tableSelector.getValue() != null)
            showTableData(tableSelector.getValue());
    }

    @FXML
    private void executeAction()
    {
        execute(SQLUtility.xargs(sqlField.getText()), null);
    }

    @FXML
    private void newAction()
    {
        File selected = AlertUtility.saveDatabaseDialog(stage,
                resources.getString("filechooser.New"));
        if (selected != null)
            openDatabase(selected);
    }

    @FXML
    private void openAction()
    {
        File selected = AlertUtility.openDatabaseDialog(stage,
                resources.getString("filechooser.Open"));
        if (selected != null)
            openDatabase(selected);
    }

    @FXML
    private void closeAction()
    {
        closeDatabase();
    }

    @FXML
    public void importAction()
    {
        // TODO
    }

    @FXML
    public void exportAction()
    {
        // TODO
    }

    @FXML
    private void showAbout()
    {
        AlertUtility.infoDialog(
                resources.getString("dialog.about.Title"),
                resources.getString("dialog.about.Content"));
    }

    @FXML
    private void onShowResultMenu()
    {
        final boolean noTableSelected = tableSelector.getValue() == null
                || tableSelector.getValue().isEmpty();
        final boolean noCellSelected = resultTable.getSelectionModel().isEmpty();

        resultMenu.getItems().forEach(item -> item.setDisable(noTableSelected));
        deleteRowMenu.setDisable(noTableSelected || noCellSelected);
        copySelectionMenu.setDisable(noCellSelected);
    }

    @FXML
    private void tableRename()
    {
        String newName = AlertUtility.inputDialog(
                resources.getString("table.Rename"),
                resources.getString("dialog.tablename.Content"));
        if (newName == null || newName.isEmpty()) return;

        execute(SQLUtility.renameTableCommand(tableSelector.getValue(), newName), newName);
        loadTables();
        tableSelector.getSelectionModel().select(newName);
    }

    @FXML
    private void tableAddRow()
    {
        String table = tableSelector.getValue();
        String[] columns = resultTable.getColumns().stream()
                .map(TableColumnBase::getText).toArray(String[]::new);
        Object[] values = AlertUtility.rowDialog(table, columns);
        if (values != null) {
            execute(SQLUtility.insertCommand(table, values), null);
            showTableData(table);
        }
    }

    @FXML
    private void tableAddColumn()
    {
        Pair<String, String> column = AlertUtility.columnDialog(
                resources.getString("dialog.newcolumn.Title"),
                resources.getString("dialog.newcolumn.Name"),
                resources.getString("dialog.newcolumn.SQL"));
        if (column == null || column.getKey().isEmpty() || column.getValue().isEmpty()) return;

        String table = tableSelector.getValue();
        execute(SQLUtility.addColumn(table, column.getKey(), column.getValue()), table);
        showTableData(table);
    }

    @FXML
    private void tableCopySelection()
    {
        if (resultTable.getSelectionModel().isEmpty()) return;

        ObservableList<TablePosition> posList = resultTable.getSelectionModel()
                .getSelectedCells();

        int oldRow = -1;
        StringBuilder clipboardString = new StringBuilder();
        for (TablePosition p : posList) {
            int r = p.getRow();
            if (oldRow == r) clipboardString.append('\t');
            else if (oldRow != -1) clipboardString.append('\n');

            clipboardString.append(resultTable.getColumns().get(p.getColumn()).getCellData(r));
            oldRow = r;
        }
        final ClipboardContent content = new ClipboardContent();
        content.putString(clipboardString.toString());
        Clipboard.getSystemClipboard().setContent(content);
    }

    @FXML
    private void tableDeleteSelection()
    {
        if (resultTable.getSelectionModel().isEmpty() || !AlertUtility.questionDialog(
                resources.getString("dialog.deleteRow.Title"),
                resources.getString("dialog.deleteRow.Content")))
            return;

        List<IdRow> items = resultTable.getSelectionModel().getSelectedItems();
        int[] ids = items.stream().mapToInt(IdRow::getId).toArray();

        final String table = tableSelector.getValue();
        execute(SQLUtility.deleteWithId(table, ids), table);
        showTableData(table);
    }

    @FXML
    private void tableSelectAll()
    {
        resultTable.getSelectionModel().selectAll();
    }

    private void showTableData(String name)
    {
        execute(SQLUtility.selectTable(name), name);
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Additional Methods">
    private void openDatabase(File file)
    {
        closeDatabase();

        recentFiles.open(file);
        stage.setTitle(recentFiles.current().getName());
        loadTables();
    }

    private void closeDatabase()
    {
        resultTable.getColumns().clear();
        tableSelector.getItems().clear();
        stage.setTitle("");
    }

    /**
     * Executes the SQL statement and fills the TableView with it's result.
     *
     * @param command standard SQL Statement
     * @param table   Currently selected table
     */
    private void execute(String command, String table)
    {
        if (command.isEmpty()) {
            resultTable.getColumns().clear();
            return;
        }

        if (recentFiles.current() == null || !recentFiles.current().exists()) {
            statusLabel.setText(resources.getString("error.fileopen"));
            return;
        }

        executeCommand(command, table);
    }

    private void executeCommand(String command, String table)
    {
        try (SQLiteInterface sql = new SQLiteInterface(recentFiles.current().getPath())) {
            SQLResult result = sql.execute(command);

            if (result.isQuery) {
                fillTable(result.columns, result.result, table != null);
                updateTableStatus();
            }
            else {
                statusLabel.setText(String.format(
                        resources.getString("execute.edit"),
                        result.updateCount));
                openDatabase(recentFiles.current());
            }
        }
        catch (SQLException e) {
            AlertUtility.exceptionDialog(resources.getString("error.execute"), e);
            statusLabel.setText(resources.getString("error.execute"));
        }

        tableSelector.getSelectionModel().select(table);
    }

    private void updateTableStatus() {
        int count = resultTable.getItems().size();
        String status = String.format(resources.getString("execute.loadAll"), count);
        statusLabel.setText(status);
    }

    private void fillTable(String[] columns, Stream<Object[]> result, boolean editable)
    {
        resultTable.getColumns().clear();

        for (int i = (editable ? 1 : 0); i < columns.length; i++) {
            TableColumn<IdRow, Object> col = new TableColumn<>(columns[i]);

            final int j = editable ? i - 1 : i;
            col.setCellValueFactory(param -> param.getValue().property(j));

            if (editable) {
                col.setCellFactory(TextFieldTableCell.forTableColumn(new NullStringConverter()));
                col.setOnEditCommit(this::onTableCellEdit);
            }
            resultTable.getColumns().add(col);
        }

        resultTable.setItems(result.limit(MAX_ROW_COUNT)
                .map((values) -> new IdRow(editable, values))
                .collect(Collectors.collectingAndThen(Collectors.toList(),
                        FXCollections::observableList)));
    }


    private void onTableCellEdit(TableColumn.CellEditEvent<IdRow, Object> event)
    {
        String currTable = tableSelector.getValue();
        String column = event.getTableColumn().getText();
        String value = String.valueOf(event.getNewValue());

        String rowCondition = SQLUtility.getIdCondition(event.getRowValue().getId());
        String sql = SQLUtility.updateCommand(currTable, value, column, rowCondition);

        execute(sql, currTable);
        showTableData(currTable);
    }

    private void loadTables()
    {
        try {
            tableSelector.getItems().setAll(
                    SQLiteInterface.getTables(recentFiles.current().getAbsolutePath()));
        }
        catch (SQLException e) {
            tableSelector.getItems().clear();
            AlertUtility.exceptionDialog(resources.getString("error.load"), e);
            statusLabel.setText(resources.getString("error.load"));
        }
    }
    // </editor-fold>
}
