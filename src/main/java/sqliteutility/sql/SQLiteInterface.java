/*
 * Copyright (C) 2016 Lars Wrenger
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package sqliteutility.sql;

import java.sql.*;
import java.util.*;
import java.util.stream.*;

/**
 * Utility for SQLite-Databases.
 *
 * @author Lars Wrenger
 */
public class SQLiteInterface
        implements AutoCloseable
{
    private final Connection c;
    private final Statement stmt;

    /**
     * Connect to the SQLite Driver and opens a connection to the database.
     *
     * @param path path of the database file
     *
     * @throws SQLException if the connection fails
     */
    public SQLiteInterface(String path)
            throws SQLException
    {
        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:" + path);
            stmt = c.createStatement();
        }
        catch (ClassNotFoundException ex) {
            throw new SQLException(ex);
        }
    }

    /**
     * Returns the names of all tables included in this database.
     *
     * @param path database location
     *
     * @return Table names
     */
    public static List<String> getTables(String path)
            throws SQLException
    {
        try (SQLiteInterface sql = new SQLiteInterface(path)) {
            SQLResult rs = sql.executeQuery(
                    "SELECT name FROM sqlite_master WHERE type='table' ORDER BY name");
            return rs.result.map(e -> (String) e[0]).collect(Collectors.toList());
        }
    }

    /**
     * Executes the given SQL statement and returns a single {@link ResultSet} if the
     * statement is a query.
     *
     * @param command any SQL statement
     *
     * @return result if the sql statement
     *
     * @throws SQLException if a database error occurs
     */
    public SQLResult execute(String command)
            throws SQLException
    {
        stmt.execute(command);

        ResultSet rs = stmt.getResultSet();
        if (rs == null) {
            return new SQLResult(null, null, stmt.getUpdateCount());
        }
        else {
            return new SQLResult(columnNames(rs), toStream(rs), -1);
        }
    }

    public SQLResult executeQuery(String query)
            throws SQLException
    {

        ResultSet rs = stmt.executeQuery(query);
        return new SQLResult(columnNames(rs), toStream(rs), -1);
    }

    private String[] columnNames(ResultSet result)
            throws SQLException
    {
        int count = result.getMetaData().getColumnCount();

        String[] columns = new String[count];
        for (int i = 0; i < count; i++) {
            columns[i] = result.getMetaData().getColumnLabel(i + 1);
        }
        return columns;
    }

    private Stream<Object[]> toStream(ResultSet result)
            throws SQLException
    {
        return StreamSupport.stream(
                Spliterators.spliteratorUnknownSize(new ResultSetIterator(result),
                        0), false);
    }


    public int executeUpdate(String command)
            throws SQLException
    {
        return stmt.executeUpdate(command);
    }

    /**
     * Releases database and JDBC resources immediately instead of waiting for them
     * to be automatically released. Prevents the database from getting locked.
     */
    @Override
    public void close()
    {
        try {
            c.close();
            stmt.close();
        }
        catch (SQLException ignored) {}
    }
}