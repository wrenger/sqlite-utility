package sqliteutility.sql;

import java.util.stream.Stream;

public class SQLResult
{
    public final boolean isQuery;
    public final String[] columns;
    public final Stream<Object[]> result;
    public final int updateCount;

    SQLResult(String[] columns, Stream<Object[]> result, int updateCount)
    {
        isQuery = result != null;
        this.columns = columns;
        this.result = result;
        this.updateCount = updateCount;
    }
}
